## Tout est dans le 👉 [Wiki](https://codefirst.iut.uca.fr/git/remi.lavergne/ULAC/wiki) 👈



# Liens Importants

Voici une liste de liens importants à votre disposition :

- 🔗 [**ENT (Environnement Numérique de Travail)**](https://ent.uca.fr/core/home/)
  - Plateforme d'accès à l'Environnement Numérique de Travail de l'Université.

- 📖 [**Moodle**](https://ent.uca.fr/moodle/my)
  - La plupart de vos cours, TD et TP sont içi.

- 📚 [**Odin**](https://odin.iut.uca.fr/)
  - Portail du Departement Informatique (Emploi du temps, notes, etc...).

- 🏠 [**Homeweb**](https://homeweb.iut.uca.fr/)
  - Connectez-vous à votre home et transférer des fichiers depuis chez vous.

- 📁 [**Opale**](https://opale.iut.uca.fr/)
  - Tout sur le système et réseau (tuto et cours).

- 💻 [**CodeFirst**](https://codefirst.iut.uca.fr/home)
  - CodeFirst la plateforme pour Gitea, Drone, Sonar, etc...

- 🌐 [**Guacamole**](https://guacamole.dsi.uca.fr/iut/#)
  - Connexion aux PC de l'IUT à distance (mode graphique).

# Commandes Utiles

Voici une liste de commandes utiles à votre disposition :

**Accès à la passerelle Tokyo:**
*Avoir setup l'authorized_keys avant*
```bash
ssh <identifiant>@ssh.iut-clermont.uca.fr
```

**Scan les PC de disponible sur le réseau:** (Depuis tokyo)
```bash
~gudavala/bin/iut-scan
```

**Connexion à un PC de l'IUT allumé:** (Depuis tokyo)
```bash
ssh <nom_du_pc>
```
Décomposition du nom des PC:
- `iutclinf<salle><numéro_du_pc><si linux alors 'l'>`: iutclinfb1004l